package com.dharbor.newsservice.exception;

/**
 * @author Ma. Laura Chiri
 */
public class UserNotFoundException extends ApplicationException {
    public UserNotFoundException(String message){
        super(message);
    }
}
