package com.dharbor.newsservice.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author Ma. Laura Chiri
 */
@Getter
@AllArgsConstructor
public class ErrorResponse {

    private String message;

    private String statusCode;

    private String url;

    private String timestamp;
}