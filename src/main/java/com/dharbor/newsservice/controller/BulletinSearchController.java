package com.dharbor.newsservice.controller;

import com.dharbor.newsservice.client.users.UserResponse;
import com.dharbor.newsservice.client.users.UserService;
import com.dharbor.newsservice.command.AttachmentCreateCmd;
import com.dharbor.newsservice.command.BulletinSearchCmd;
import com.dharbor.newsservice.model.Pagination;
import com.dharbor.newsservice.model.builder.AttachmentResponseBuilder;
import com.dharbor.newsservice.model.builder.BulletinResponseBuilder;
import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.input.SearchContentInput;
import com.dharbor.newsservice.model.response.AttachmentResponse;
import com.dharbor.newsservice.model.response.BulletinResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ma. Laura Chiri
 */
@RequestMapping(value = "/search")
@RequestScope
@RestController
public class BulletinSearchController {

    private BulletinSearchCmd command;

    private UserService userService;

    public BulletinSearchController(BulletinSearchCmd command, UserService userService) {
        this.command = command;
        this.userService = userService;
    }

    @Autowired
    AttachmentCreateCmd attachmentCreateCmd;

    @PostMapping(value = "/{userId}")
    public Pagination<BulletinResponse> readBulletin(@RequestHeader("User-ID") Long userId,
                                                     @RequestHeader("Account-ID") Long accountId,
                                                     @RequestParam("Limit") Integer limit,
                                                     @RequestParam("Page") Integer page,
                                                     @RequestBody SearchContentInput content) {

        command.setUserId(userId);
        command.setLimit(limit);
        command.setPage(page);
        command.setInput(content);
        command.execute();

        List<Bulletin> bulletins = command.getBulletins();
        List<BulletinResponse> bulletinsResponses = buildBulletinResponse(bulletins);

        Pagination<BulletinResponse> pagination = new Pagination<>();
        pagination.setContent(bulletinsResponses);
        pagination.setTotalPages(command.getTotalPages());
        pagination.setTotalElements(Long.valueOf(command.getTotalElements()));
        return pagination;
    }

    private List<BulletinResponse> buildBulletinResponse(List<Bulletin> bulletins) {
        return bulletins.stream().map(bulletin -> BulletinResponseBuilder.getInstance(bulletin).
                setAttachments(buildAttachmentResponse(attachmentCreateCmd.getFiles(bulletin))).
                setUserResponse(userService.readById(bulletin.getSenderUserId())).build()).
                collect(Collectors.toList());
    }

    private List<AttachmentResponse> buildAttachmentResponse(List<Attachment> attachments) {
        return attachments.stream().map(attachment -> AttachmentResponseBuilder.getInstance(attachment).
                build())
                .collect(Collectors.toList());
    }

}

