package com.dharbor.newsservice.controller;

import com.dharbor.newsservice.client.users.UserService;
import com.dharbor.newsservice.command.AttachmentCreateCmd;
import com.dharbor.newsservice.command.BulletinListCmd;
import com.dharbor.newsservice.model.Pagination;
import com.dharbor.newsservice.model.builder.AttachmentResponseBuilder;
import com.dharbor.newsservice.model.builder.BulletinResponseBuilder;
import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.response.AttachmentResponse;
import com.dharbor.newsservice.model.response.BulletinResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ma. Laura Chiri
 */
@RequestMapping(value = "/bulletins")
@RequestScope
@RestController
public class BulletinListController {

    private BulletinListCmd bulletinListCmd;

    private UserService userService;

    public BulletinListController(BulletinListCmd bulletinListCmd, UserService userService) {
        this.bulletinListCmd = bulletinListCmd;
        this.userService = userService;
    }

    @Autowired
    AttachmentCreateCmd attachmentCreateCmd;

    @GetMapping(value = "/list")
    public List<BulletinResponse> readBulletin(@RequestHeader("User-ID") Long userId,
                                                     @RequestHeader("Account-ID") Long accountId)
    {

        bulletinListCmd.setUserId(userId);
        bulletinListCmd.execute();

        List<Bulletin> bulletins = bulletinListCmd.getBulletins();

        return bulletins.stream().map(bulletin -> BulletinResponseBuilder.getInstance(bulletin).
                setAttachments(buildAttachmentResponse(attachmentCreateCmd.getFiles(bulletin))).
                setUserResponse(userService.readById(bulletin.getSenderUserId())).build()).
                collect(Collectors.toList());
    }

    private List<AttachmentResponse> buildAttachmentResponse(List<Attachment> attachments) {
        return attachments.stream().map(attachment -> AttachmentResponseBuilder.getInstance(attachment).
                build())
                .collect(Collectors.toList());
    }

}

