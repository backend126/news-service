package com.dharbor.newsservice.controller;

import com.dharbor.newsservice.command.BulletinCreateCmd;
import com.dharbor.newsservice.model.builder.AttachmentResponseBuilder;
import com.dharbor.newsservice.model.builder.BulletinResponseBuilder;
import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.input.BulletinCreateInput;
import com.dharbor.newsservice.model.response.AttachmentResponse;
import com.dharbor.newsservice.model.response.BulletinResponse;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ma. Laura Chiri
 */
@RestController
@RequestScope
@RequestMapping("/bulletins")
public class BulletinCreateController {

    private BulletinCreateCmd bulletinCreateCmd;

    public BulletinCreateController(BulletinCreateCmd bulletinCreateCmd) {
        this.bulletinCreateCmd = bulletinCreateCmd;
    }

    @PostMapping
    public BulletinResponse createBulletin(@RequestHeader("Account-ID") Long accountId,
                                           @RequestHeader("User-ID") Long userId,
                                           @RequestBody BulletinCreateInput input) {

        bulletinCreateCmd.setAccountId(accountId);
        bulletinCreateCmd.setUserId(userId);
        bulletinCreateCmd.setInput(input);
        bulletinCreateCmd.execute();

        Bulletin bulletin = bulletinCreateCmd.getBulletin();
        List<AttachmentResponse> attachments = buildAttachments(bulletinCreateCmd.getAttachments());

        return BulletinResponseBuilder.getInstance(bulletin).
                setAttachments(attachments).
                setUserResponse(bulletinCreateCmd.getUser()).build();
    }

    private List<AttachmentResponse> buildAttachments(List<Attachment> attachments) {
        return attachments.stream().map(file ->
                AttachmentResponseBuilder.getInstance(file).build()).collect(Collectors.toList());
    }
}
