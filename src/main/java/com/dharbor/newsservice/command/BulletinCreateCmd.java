package com.dharbor.newsservice.command;

import com.dharbor.newsservice.client.users.UserResponse;
import com.dharbor.newsservice.client.users.UserService;
import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.input.BulletinCreateInput;
import com.dharbor.newsservice.model.repositories.BulletinRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommandFactory;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@SynchronousExecution
public class BulletinCreateCmd implements BusinessLogicCommand {

    @Setter
    private Long accountId;

    @Setter
    private Long userId;

    @Setter
    private BulletinCreateInput input;

    @Getter
    private Bulletin bulletin;

    @Getter
    private List<Attachment> attachments;

    @Getter
    private UserResponse user;

    protected UserService userService;


    protected BulletinRepository bulletinRepository;

    private BusinessLogicCommandFactory commandFactory;

    public BulletinCreateCmd(UserService userService, BulletinRepository bulletinRepository,
                             BusinessLogicCommandFactory commandFactory) {
        this.userService = userService;
        this.bulletinRepository = bulletinRepository;
        this.commandFactory = commandFactory;
    }

    @Override
    public void execute() {

        user = userService.readById(userId);

        bulletin = bulletinRepository.save(composeBulletin(input));

        if (!CollectionUtils.isEmpty(input.getFileIds())) {
            attachments = createAttachments(bulletin, input.getFileIds());
        }
    }

    private Bulletin composeBulletin(BulletinCreateInput input) {
        Bulletin instance = new Bulletin();
        instance.setSenderUserId(userId);
        instance.setBody(input.getContent());

        return instance;
    }

    private List<Attachment> createAttachments(Bulletin bulletin, List<String> fileIds) {
        AttachmentCreateCmd command = commandFactory.createInstance(AttachmentCreateCmd.class);
        command.setAccountId(accountId);
        command.setBulletin(bulletin);
        command.setFileIds(fileIds);
        command.execute();

        return command.getAttachments();
    }
}
