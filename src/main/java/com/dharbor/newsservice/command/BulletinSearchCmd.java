package com.dharbor.newsservice.command;

import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.domain.Bulletin_;
import com.dharbor.newsservice.model.input.SearchContentInput;
import com.dharbor.newsservice.model.repositories.BulletinRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@SynchronousExecution
public class BulletinSearchCmd implements BusinessLogicCommand {

    @Setter
    private Long userId;

    @Setter
    private Integer limit;

    @Setter
    private Integer page;

    @Setter
    private SearchContentInput input;

    @Getter
    private Integer totalPages;

    @Getter
    private long totalElements;

    @Getter
    private List<Bulletin> bulletins;

    private BulletinRepository bulletinRepository;

    public BulletinSearchCmd(BulletinRepository bulletinRepository) {
        this.bulletinRepository = bulletinRepository;
    }

    @Override
    public void execute() {

        bulletins = new ArrayList<>();

        PageRequest pageRequest = PageRequest.of(page, limit);
        Page<Bulletin> pageResult = bulletinRepository.findAll(buildSpecification(), pageRequest);

        List<Bulletin> content = pageResult.getContent();
        if (!CollectionUtils.isEmpty(content)) {
            bulletins.addAll(content);
        }

        totalPages = pageResult.getTotalPages();
        totalElements = pageResult.getTotalElements();
    }

    private Specification<Bulletin> buildSpecification() {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

//            predicates.add(cb.equal(root.get(Bulletin_.senderUserId), userId));

            if (!StringUtils.isEmpty(input.getContent())) {
                predicates.add(cb.like(root.get(Bulletin_.body), "%" + input.getContent() + "%"));
            }

            query.orderBy(cb.desc(root.get(Bulletin_.id)));

            return cb.and(predicates.toArray(new Predicate[0]));
        };
    }
}
