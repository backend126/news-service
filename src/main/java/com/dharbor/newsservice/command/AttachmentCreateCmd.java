package com.dharbor.newsservice.command;

import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.repositories.AttachmentRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Ma. Laura Chiri
 */
@SynchronousExecution
public class AttachmentCreateCmd implements BusinessLogicCommand {

    @Setter
    private Long accountId;

    @Setter
    protected Bulletin bulletin;

    @Setter
    private List<String> fileIds;

    @Getter
    private List<Attachment> attachments;

    @Getter
    private List<Attachment> files;

    protected AttachmentRepository repository;

    public AttachmentCreateCmd(AttachmentRepository repository) {
        this.repository = repository;
    }

    @Override
    public void execute() {

        attachments = repository.saveAll(composeAttachmentList(bulletin, fileIds));
    }


    private List<Attachment> composeAttachmentList(Bulletin bulletin, List<String> fileIds) {
        return fileIds.stream()
                .map(fileId -> buildAttachmentInstance(bulletin, fileId))
                .collect(Collectors.toList());
    }

    private Attachment buildAttachmentInstance(Bulletin bulletin, String fileId) {
        Attachment instance = new Attachment();
        instance.setAccountId(accountId);
        instance.setBulletin(bulletin);
        instance.setFileId(fileId);

        return instance;
    }

    public List<Attachment> getFiles(Bulletin bulletin) {
        Long bulletinId = bulletin.getId();
        files = repository.findByBulletinId(bulletinId);

        return files;
    }
}
