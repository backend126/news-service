package com.dharbor.newsservice.command;

import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.domain.Bulletin_;
import com.dharbor.newsservice.model.repositories.BulletinRepository;
import com.jatun.open.tools.blcmd.annotations.SynchronousExecution;
import com.jatun.open.tools.blcmd.core.BusinessLogicCommand;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.CollectionUtils;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@SynchronousExecution
public class BulletinListCmd implements BusinessLogicCommand {

    @Setter
    private Long userId;

    @Getter
    private List<Bulletin> bulletins;

    private BulletinRepository bulletinRepository;

    public BulletinListCmd(BulletinRepository bulletinRepository) {
        this.bulletinRepository = bulletinRepository;
    }

    @Override
    public void execute() {

        bulletins = bulletinRepository.findAll();
    }
}
