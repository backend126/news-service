package com.dharbor.newsservice.model.repositories;

import com.dharbor.newsservice.model.domain.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
    List<Attachment> findByBulletinId(Long id);
}
