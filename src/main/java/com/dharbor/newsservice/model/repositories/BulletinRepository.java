package com.dharbor.newsservice.model.repositories;

import com.dharbor.newsservice.model.domain.Bulletin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
public interface BulletinRepository extends JpaRepository<Bulletin, Long>, JpaSpecificationExecutor<Bulletin> {

    List<Bulletin> findBySenderUserId(Long userId);
}
