package com.dharbor.newsservice.model.builder;

import com.dharbor.newsservice.client.users.UserResponse;
import com.dharbor.newsservice.model.domain.Bulletin;
import com.dharbor.newsservice.model.response.AttachmentResponse;
import com.dharbor.newsservice.model.response.BulletinResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
public class BulletinResponseBuilder {

    private BulletinResponse instance;

    private UserResponse user;

    private List<AttachmentResponse> attachments;

    public static BulletinResponseBuilder getInstance(Bulletin bulletin) {
        return (new BulletinResponseBuilder()).setBulletin(bulletin);
    }

    private BulletinResponseBuilder() {

        instance = new BulletinResponse();
        user = new UserResponse();
        attachments = new ArrayList<>();
    }

    private BulletinResponseBuilder setBulletin(Bulletin bulletin) {
        instance.setBulletinId(bulletin.getId());
        instance.setAccountId(bulletin.getAccountId());
        instance.setSenderUserId(bulletin.getSenderUserId());
        instance.setBody(bulletin.getBody());
        instance.setCreatedDate(bulletin.getCreatedDate());
        instance.setIsDeleted(bulletin.getIsDeleted());
        instance.setCommentsCounter(bulletin.getCommentsCounter());

        return this;
    }

    public BulletinResponseBuilder setUserResponse(UserResponse user) {
        instance.setSenderUser(user);
        this.user = user;

        return this;
    }

    public BulletinResponseBuilder setAttachments(List<AttachmentResponse> attachments) {
        instance.setAttachments(attachments);
        this.attachments = attachments;

        return this;
    }

    public BulletinResponse build() {
        return instance;
    }
}
