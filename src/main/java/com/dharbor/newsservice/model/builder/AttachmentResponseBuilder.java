package com.dharbor.newsservice.model.builder;

import com.dharbor.newsservice.model.domain.Attachment;
import com.dharbor.newsservice.model.response.AttachmentResponse;

/**
 * @author Ma. Laura Chiri
 */
public class AttachmentResponseBuilder {

    private AttachmentResponse instance;

    public static AttachmentResponseBuilder getInstance(Attachment attachment) {
        return (new AttachmentResponseBuilder()).setAttachment(attachment);
    }

    private AttachmentResponseBuilder() {
        this.instance = new AttachmentResponse();
    }

    private AttachmentResponseBuilder setAttachment(Attachment attachment) {
        instance.setFileId(attachment.getFileId());
        instance.setName(attachment.getName());
        instance.setMimeType(attachment.getMimeType());
        instance.setSize(attachment.getSize());
        return this;
    }

    public AttachmentResponse build() {
        return instance;
    }
}
