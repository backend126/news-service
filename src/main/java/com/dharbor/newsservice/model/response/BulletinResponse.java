package com.dharbor.newsservice.model.response;

import com.dharbor.newsservice.client.users.UserResponse;
import com.dharbor.newsservice.model.domain.Attachment;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@Getter
@Setter
public class BulletinResponse implements Serializable {

    private Long bulletinId;

    private Long accountId;

    private Long senderUserId;

    private String body;

    private Date createdDate;

    private Boolean isDeleted;

    private Integer commentsCounter;

    private UserResponse senderUser;

    private List<AttachmentResponse> attachments;

}
