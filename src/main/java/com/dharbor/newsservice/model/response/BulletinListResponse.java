package com.dharbor.newsservice.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
public class BulletinListResponse implements Serializable {

    private Long bulletinId;

    private Long accountId;

    private Long senderUserId;

    private String body;

    private Date createdDate;

    private Boolean isDeleted;

    private Integer commentsCounter;

}
