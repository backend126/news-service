package com.dharbor.newsservice.model.response;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
public class AttachmentResponse implements Serializable {

    private String fileId;

    private Integer size;

    private String name;

    private String mimeType;
}
