package com.dharbor.newsservice.model.input;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Ma. Laura Chiri
 */
@Getter
@Setter
public class SearchContentInput {

    private String content;
}
