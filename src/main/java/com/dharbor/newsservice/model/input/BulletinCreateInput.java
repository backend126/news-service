package com.dharbor.newsservice.model.input;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@Getter
@Setter
public class BulletinCreateInput {

    private String content;

    private List<String> fileIds;

}
