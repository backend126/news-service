package com.dharbor.newsservice.model.domain;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import java.util.List;

/**
 * @author Ma. Laura Chiri
 */
@StaticMetamodel(Bulletin.class)
public class Bulletin_ {
    public static volatile SingularAttribute<Bulletin, Long> id;

    public static volatile SingularAttribute<Bulletin, Long> senderUserId;

    public static volatile SingularAttribute<Bulletin, String> body;

    public static volatile SingularAttribute<Bulletin, List<String>> fileIds;
}