package com.dharbor.newsservice.model.domain;

/**
 * @author Ma. Laura Chiri
 */
public final class Constants {
    private Constants() {
    }

    public static class BulletinTable {
        public static final String NAME = "bulletin_table";

        public static class Id {
            public static final String NAME = "bulletinid";
        }

        public static class AccountId {
            public static final String NAME = "accountid";
        }

        public static class SenderUserId {
            public static final String NAME = "senderuserid";
        }

        public static class Body {
            public static final String NAME = "body";
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }

        public static class IsDeleted {
            public static final String NAME = "isdeleted";
        }

        public static class CommentsCounter {
            public static final String NAME = "commentscounter";
        }
    }

    public static class CommentTable {
        public static final String NAME = "comment_table";

        public static class Id {
            public static final String NAME = "commentid";
        }

        public static class AccountId {
            public static final String NAME = "accounttid";
        }

        public static class SenderUserId {
            public static final String NAME = "senderuserid";
        }

        public static class Content {
            public static final String NAME = "content";
        }

        public static class RepliesCounter {
            public static final String NAME = "repliescounter";
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }

        public static class IsDeleted {
            public static final String NAME = "isdeleted";
        }

        public static class BulletinId {
            public static final String NAME = "bulletinid";
        }
    }

    public static class AttachmentTable {
        public static final String NAME = "attachment_table";

        public static class Id {
            public static final String NAME = "attachmentid";
        }

        public static class AccountId {
            public static final String NAME = "accountid";
        }

        public static class FileId {
            public static final String NAME = "fileid";
        }

        public static class Size {
            public static final String NAME = "size";
        }

        public static class Name {
            public static final String NAME = "name";
        }

        public static class MimeType {
            public static final String NAME = "mimetype";
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }

        public static class IsDeleted {
            public static final String NAME = "isdeleted";
        }

        public static class BulletinId {
            public static final String NAME = "bulletinid";
        }
    }

}
