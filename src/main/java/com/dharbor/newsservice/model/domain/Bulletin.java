package com.dharbor.newsservice.model.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.dharbor.newsservice.model.domain.Constants.BulletinTable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
@Entity
@Table(
        name = BulletinTable.NAME
)
public class Bulletin implements Serializable {

    @Id
    @Column(name= BulletinTable.Id.NAME)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = BulletinTable.AccountId.NAME)
    private Long accountId;

    @Column(name = BulletinTable.SenderUserId.NAME)
    private Long senderUserId;

    @Column(name = BulletinTable.Body.NAME)
    private String body;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = BulletinTable.CreatedDate.NAME)
    private Date createdDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = BulletinTable.IsDeleted.NAME)
    private Boolean isDeleted;

    @Column(name = BulletinTable.CommentsCounter.NAME)
    private Integer commentsCounter;

    @PrePersist
    void onPrePersist(){
        this.createdDate = new Date();
        this.isDeleted = false;
    }
}
