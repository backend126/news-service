package com.dharbor.newsservice.model.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.dharbor.newsservice.model.domain.Constants.CommentTable;
import static com.dharbor.newsservice.model.domain.Constants.BulletinTable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
@Entity
@Table(
        name = CommentTable.NAME
)
public class Comment implements Serializable {
    @Id
    @Column(name = CommentTable.Id.NAME)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = CommentTable.AccountId.NAME)
    private Long accountId;

    @Column(name = CommentTable.SenderUserId.NAME)
    private Long senderUserId;

    @Column(name = CommentTable.Content.NAME)
    private String content;

    @Column(name = CommentTable.RepliesCounter.NAME)
    private Integer repliesCounter;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = CommentTable.CreatedDate.NAME)
    private Date createdDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = CommentTable.IsDeleted.NAME)
    private Boolean isDeleted;

    @ManyToOne(fetch =FetchType.LAZY)
    @JoinColumn(name = CommentTable.BulletinId.NAME, referencedColumnName = BulletinTable.Id.NAME, nullable = false)
    private Bulletin bulletin;

    @PrePersist
    void onPrePersist(){
        this.createdDate = new Date();
        this.isDeleted = false;
    }
}
