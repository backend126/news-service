package com.dharbor.newsservice.model.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.dharbor.newsservice.model.domain.Constants.AttachmentTable;
import static com.dharbor.newsservice.model.domain.Constants.BulletinTable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
@Entity
@Table(
        name = AttachmentTable.NAME
)
public class Attachment implements Serializable {

    @Id
    @Column(name= AttachmentTable.Id.NAME)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = AttachmentTable.AccountId.NAME)
    private Long accountId;

    @Column(name = AttachmentTable.FileId.NAME)
    private String fileId;

    @Column(name = AttachmentTable.Size.NAME)
    private Integer size;

    @Column(name = AttachmentTable.Name.NAME)
    private String name;

    @Column(name = AttachmentTable.MimeType.NAME)
    private String mimeType;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = AttachmentTable.CreatedDate.NAME)
    private Date createdDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = AttachmentTable.IsDeleted.NAME)
    private Boolean isDeleted;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = AttachmentTable.BulletinId.NAME, referencedColumnName = BulletinTable.Id.NAME, nullable = false)
    private Bulletin bulletin;

    @PrePersist
    void onPrePersist(){
        this.size = 0;
        this.name = "file";
        this.mimeType = "mimeType";
        this.createdDate = new Date();
        this.isDeleted = false;
    }
}
