package com.dharbor.newsservice.client.users.domain;


import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

import static com.dharbor.newsservice.client.users.domain.Constants.UserTable;

/**
 * @author Ma. Laura Chiri
 */
@Getter
@Setter
@Entity
@Table(
        name = UserTable.NAME
)
public class User implements Serializable {

    @Id
    @Column(name = UserTable.Id.NAME, nullable=false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = UserTable.AccountId.NAME)
    private Long accountId;

    @Column(name = UserTable.FirstName.NAME, length = 100, nullable = false)
    private String firstName;

    @Column(name = UserTable.LastName.NAME, length = 100, nullable = false)
    private String lastName;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = UserTable.CreatedDate.NAME)
    private Date createdDate;

    @Type(type = "org.hibernate.type.NumericBooleanType")
    @Column(name = UserTable.IsDeleted.NAME)
    private Boolean isDeleted;

    @PrePersist
    void onPrePersist() {
        this.createdDate = new Date();
        this.isDeleted = Boolean.FALSE;
    }
}
