package com.dharbor.newsservice.client.users;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author Ma. Laura Chiri
 */
@Setter
@Getter
public class UserResponse implements Serializable {

    private String firstName;

    private String lastName;

    private Long accountId;

    private Long id;

}
