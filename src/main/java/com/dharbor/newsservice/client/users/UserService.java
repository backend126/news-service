package com.dharbor.newsservice.client.users;

import com.dharbor.newsservice.exception.UserNotFoundException;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Ma. Laura Chiri
 */

@Service
public class UserService {

    @Autowired
    private UserClient client;

    @HystrixCommand
    public UserResponse readById(Long userId) {

        try {
            return client.readById(userId);
        } catch (Exception e) {
            throw new UserNotFoundException("User not found by id: " + userId);
        }

    }

}