package com.dharbor.newsservice.client.users;

import com.dharbor.newsservice.client.users.domain.User;

/**
 * @author Ma. Laura Chiri
 */
public final class UserResponseBuilder {

    private UserResponse instance;

    public static UserResponseBuilder getInstance(User user) {
        return(new UserResponseBuilder()).setUser(user);
    }

    private UserResponseBuilder() {
        this.instance = new UserResponse();
    }

    private UserResponseBuilder setUser(User user){
        instance.setId(user.getId());
        instance.setAccountId(user.getAccountId());
        instance.setFirstName(user.getFirstName());
        instance.setLastName(user.getLastName());

        return this;
    }

    public UserResponse build() {
        return instance;
    }
}
