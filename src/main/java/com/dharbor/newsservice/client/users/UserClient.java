package com.dharbor.newsservice.client.users;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Ma. Laura Chiri
 */
@FeignClient(name = "users-service", url = "localhost:8081")
@RequestMapping("/users")
public interface UserClient {

    @GetMapping(value="/{userId}")
    public UserResponse readById(@PathVariable("userId") Long userId);
}
