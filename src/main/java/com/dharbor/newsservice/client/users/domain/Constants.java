package com.dharbor.newsservice.client.users.domain;

/**
 * @author Ma. Laura Chiri
 */
public final class Constants {
    private Constants() {
    }

    public static class UserTable {
        public static final String NAME = "user_table";

        public static class Id {
            public static final String NAME = "userid";
        }

        public static class AccountId {
            public static final String NAME = "accountid";
        }

        public static class FirstName {
            public static final String NAME = "firstname";
        }

        public static class LastName {
            public static final String NAME = "lastname";
        }

        public static class CreatedDate {
            public static final String NAME = "createddate";
        }

        public static class IsDeleted {
            public static final String NAME = "isdeleted";
        }
    }
}
