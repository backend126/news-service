package com.dharbor.newsservice.client;

import com.dharbor.newsservice.exception.ApplicationException;

/**
 * @author Ma. Laura Chiri
 */
public class BadRequestException extends ApplicationException {
}
